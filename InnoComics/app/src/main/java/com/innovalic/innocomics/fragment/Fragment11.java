package com.innovalic.innocomics.fragment;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.GridView;
import android.widget.TextView;
import android.widget.Toast;

import com.innovalic.innocomics.R;
import com.innovalic.innocomics.activity.ComicListActivity;
import com.innovalic.innocomics.adapter.GridViewAdapter;
import com.innovalic.innocomics.model.GridViewItem;
import com.innovalic.innocomics.utils.NetworkUtil;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;

import java.util.ArrayList;

/**
 * Created by Administrator on 2016-07-04.
 */
public class Fragment11 extends Fragment implements View.OnClickListener {
    private final String TAG = " Fragment11 - ";
    private ProgressDialog mProgressDialog;
    private ArrayList<GridViewItem> gridArr;
    private final String baseUrl = "http://marumaru.in";
    private final String categoryUrl = "/?r=home&m=bbs&bid=manga&where=tag&keyword=G%3A순정";
    private String pageUrl = "&p=";

    private GridView gridView;
    GetGridView asyncGridview;

    private int pageNum = 1;
    private TextView tv_currentPage;
    private TextView tv_lastPage;
    private Button nextBtn;
    private Button preBtn;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view;
        String model = Build.MODEL.toLowerCase();
        if(model.equals("sph-d720") || model.contains("nexus") /*|| model.contains("lg")*/){
            view = inflater.inflate(R.layout.fragment02, container, false);
        } else {
            view = inflater.inflate(R.layout.fragment01, container, false);
            gridView = (GridView)view.findViewById(R.id.gridview);

            tv_currentPage = (TextView)view.findViewById(R.id.fr02_currentpage);
            tv_lastPage = (TextView)view.findViewById(R.id.fr02_lastpage);
            preBtn = (Button)view.findViewById(R.id.fr02_prebtn);
            nextBtn = (Button)view.findViewById(R.id.fr02_nextbtn);

            preBtn.setOnClickListener(this);
            nextBtn.setOnClickListener(this);

            asyncGridview = new GetGridView();//.execute();
            asyncGridview.execute();
        }

        return view;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.fr02_prebtn :
                if(pageNum != 1){
                    pageNum--;
                    //asyncGridview.execute();
                    new GetGridView().execute();
                }
                break;

            case R.id.fr02_nextbtn :
                if(pageNum != Integer.parseInt(tv_lastPage.getText().toString())){
                    pageNum++;
                    //asyncGridview.execute();
                    new GetGridView().execute();
                }
                break;
        }
    }

    public class GetGridView extends AsyncTask<Void, Void, Void> {

        String tempLastPage = "";

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            mProgressDialog = new ProgressDialog(getActivity());
            mProgressDialog.setTitle("리스트를 불러오는 중입니다.");
            mProgressDialog.setMessage("Loading...");
            mProgressDialog.setIndeterminate(false);
            mProgressDialog.show();
        }

        @Override
        protected Void doInBackground(Void... params) {

            gridArr = null;
            gridArr = new ArrayList<GridViewItem>();

            Document doc = null;

            try {
                doc = Jsoup.connect(baseUrl + categoryUrl + pageUrl + pageNum).timeout(20000).userAgent("Chrome").get();
                Elements divs = doc.select(".gallery .picbox");
                Elements pages = doc.select(".pagebox01");
                tempLastPage = pages.text().charAt(pages.text().length()-1)+"";
                if(tempLastPage.equals("0")){
                    tempLastPage = pages.text().charAt(pages.text().length()-2) + tempLastPage;
                }
                for(int i=0 ; i<divs.size() ; i++){
                    String imgUrl = divs.get(i).select("img").attr("src");
                    String title = divs.get(i).select(".sbjx").text();
                    String listUrl = divs.get(i).select(".pic a").attr("href");

                    gridArr.add(new GridViewItem(imgUrl, title, listUrl));
                }
            } catch(Exception e){
                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);

            tv_currentPage.setText(pageNum+"");
            tv_lastPage.setText(tempLastPage);

            if(gridArr==null || gridArr.size()==0){
                Toast.makeText(getActivity(), "접속자 수가 많거나 네트워크가 불안정 합니다. 다시 시도해 주세요.", Toast.LENGTH_LONG).show();
            }

            // adapter에 적용
            gridView.setAdapter(new GridViewAdapter(getActivity(), gridArr, R.layout.gridviewitem));

            gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    if(NetworkUtil.getConnectivity(getActivity())){
                        String listUrl = baseUrl + gridArr.get(position).getListUrl();
                        //Log.d(TAG, listUrl);

                        Intent intent = new Intent(getActivity(), ComicListActivity.class);
                        intent.putExtra("listUrl", listUrl);
                        intent.putExtra("imgUrl", gridArr.get(position).getImgUrl());
                        Log.d(TAG, listUrl);
                        intent.putExtra("categoryUrl", "월");
                        startActivity(intent);
                    } else {
                        Toast.makeText(getActivity(), "check your network connection status", Toast.LENGTH_SHORT).show();
                    }
                }
            });


            mProgressDialog.dismiss();
        }
    }
}
