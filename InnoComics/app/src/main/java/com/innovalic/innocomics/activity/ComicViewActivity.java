package com.innovalic.innocomics.activity;

import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import com.innovalic.innocomics.R;
import com.innovalic.innocomics.adapter.ComicViewListViewAdapter;
import com.innovalic.innocomics.adapter.ListViewAdapter;
import com.innovalic.innocomics.utils.NetworkUtil;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;

import java.util.ArrayList;

public class ComicViewActivity extends AppCompatActivity {
    private final String TAG = " ComicViewActivity - ";
    private ProgressDialog mProgressDialog;
    private ListView listview;
    private ArrayList<String> imgUrl;



    //webView.getSettings().setUserAgentString("Mozilla/5.0 (X11; U; Linux i686; en-US; rv:1.9.0.4) Gecko/20100101 Firefox/4.0");


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_comic_view);

        listview = (ListView)findViewById(R.id.comicview_listview);
        //listview.setAdapter(new ComicViewListViewAdapter(ComicViewActivity.this, ));
        new GetListView().execute();
    }



    public class GetListView extends AsyncTask<Void, Void, Void> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            mProgressDialog = new ProgressDialog(ComicViewActivity.this);
            mProgressDialog.setTitle("만화를 불러오는 중입니다.");
            mProgressDialog.setMessage("Loading...");
            mProgressDialog.setIndeterminate(false);
            mProgressDialog.show();
        }

        @Override
        protected Void doInBackground(Void... params) {

            //listArr = null;
            imgUrl = null;
            // = null;
            //listArr = new ArrayList<ListViewItem>();
            imgUrl = new ArrayList<String>();
            //playUrlArr = new ArrayList<String>();

            Document doc = null;

            try {
                doc = Jsoup.connect("http://blog.yuncomics.com/archives/1713730").timeout(10000).userAgent("Chrome").get();
                Elements divs = doc.select("body");

                Log.d(TAG, "text : " + divs.text());
                //System.out.println(divs.text());

            } catch(Exception e){
                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            // adapter에 적용


            mProgressDialog.dismiss();
        }
    }


}
