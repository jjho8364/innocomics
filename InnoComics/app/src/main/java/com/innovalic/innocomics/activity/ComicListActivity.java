package com.innovalic.innocomics.activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.innovalic.innocomics.R;
import com.innovalic.innocomics.adapter.ListViewAdapter;
import com.innovalic.innocomics.model.ListViewItem;
import com.innovalic.innocomics.utils.NetworkUtil;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;

import java.util.ArrayList;
import java.util.Locale;

public class ComicListActivity extends AppCompatActivity {

    private final String TAG = " AniListActivity -  ";
    private final String baseUrl = "http://www.marutv.com";
    private final String videoBaseUrl = "http://video2.moeni.net/";
    private String categoryUrl = "";
    private String imgUrl = "";
    private String listUrl = "";
    private ListView listView;
    private ArrayList<ListViewItem> listArr;
    private ProgressDialog mProgressDialog;
    private EditText editText;
    private ListViewAdapter adapter;

    private ArrayList<String> titleArr;
    private ArrayList<String> playUrlArr;

    private GetListView getListView;
    //private ListViewAdapter adapter;



    //AdmobAdapterWrapper adapterWrapper;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_comic_list);

        AdView mAdViewUpper = (AdView)findViewById(R.id.adView_list_upper);
        AdRequest adRequest = new AdRequest.Builder().build();
        mAdViewUpper.loadAd(adRequest);

        Intent intent = getIntent();
        //categoryUrl = (String)intent.getSerializableExtra("categoryUrl");
        listUrl = (String)intent.getSerializableExtra("listUrl");
        imgUrl = (String)intent.getSerializableExtra("imgUrl");
        //Log.d(TAG, listUrl);

        listView = (ListView)findViewById(R.id.anilist_listview);
        editText = (EditText)findViewById(R.id.anilist_edit);
        editText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                String text = editText.getText().toString().toLowerCase(Locale.getDefault());
                //adapter.filter(text);
            }
        });


        // 쓰레드 돌린다.
        getListView = new GetListView();
        getListView.execute();

    }

    public class GetListView extends AsyncTask<Void, Void, Void> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            mProgressDialog = new ProgressDialog(ComicListActivity.this);
            mProgressDialog.setTitle("리스트를 불러오는 중입니다.");
            mProgressDialog.setMessage("Loading...");
            mProgressDialog.setIndeterminate(false);
            mProgressDialog.show();
        }

        @Override
        protected Void doInBackground(Void... params) {

            //listArr = null;
            titleArr = null;
            playUrlArr = null;
            //listArr = new ArrayList<ListViewItem>();
            titleArr = new ArrayList<String>();
            playUrlArr = new ArrayList<String>();

            Document doc = null;

            try {
                doc = Jsoup.connect(listUrl).timeout(10000).userAgent("Chrome").get();
                Elements divs = doc.select("#vContent div a");

                for(int i=0 ; i<divs.size()-5 ; i++){
                    String temptitle = divs.get(i).text();
                    if(temptitle.contains("추천")){
                        break;
                    }
                    titleArr.add(divs.get(i).text());
                    playUrlArr.add(divs.get(i).attr("href"));
                    //Log.d(TAG, divs.get(i).attr("href"));
                }
            } catch(Exception e){
                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            // adapter에 적용
            adapter = new ListViewAdapter(ComicListActivity.this, imgUrl, titleArr, getLayoutInflater());
            listView.setAdapter(adapter);

            listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    if (NetworkUtil.getConnectivity(ComicListActivity.this)) {

                        // 히스토리 저장한다.



                        Intent intent = new Intent(ComicListActivity.this, VideoViewActivity.class);
                        intent.putExtra("urlPath", playUrlArr.get(position));
                        startActivity(intent);
                    } else {
                        Toast.makeText(ComicListActivity.this, "check your network connection status", Toast.LENGTH_SHORT).show();
                    }
                }
            });

            mProgressDialog.dismiss();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.d(TAG, " onRsume ");
        editText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                String text = editText.getText().toString().toLowerCase(Locale.getDefault());
                adapter.filter(text);
            }
        });
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        getListView.cancel(true);
    }
}
