package com.innovalic.innocomics.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.innovalic.innocomics.R;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.Locale;

/**
 * Created by Administrator on 2016-07-04.
 */
public class ListViewAdapter extends BaseAdapter {
    Context context;
    private LayoutInflater inflater;
    private String imgUrl;
    //ArrayList<ListViewItem> listArr;

    ArrayList<String> titleArr;
    private ArrayList<String> arraylist;
    ArrayList<String> playUrlArr;

    public ListViewAdapter(Context context, String imgUrl, ArrayList<String> titleArr, LayoutInflater inflater){
        this.context = context;
        //this.listArr = listArr;
        this.imgUrl = imgUrl;
        this.titleArr = titleArr;
        this.arraylist = new ArrayList<String>();
        arraylist.addAll(titleArr);
        this.inflater = inflater;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        convertView = inflater.inflate(R.layout.listviewitem, null);
        if(convertView != null){

            ImageView imageView = (ImageView)convertView.findViewById(R.id.listview_item_img);
            TextView tv_title = (TextView)convertView.findViewById(R.id.listview_item_title);
            //TextView tv_episode = (TextView)convertView.findViewById(R.id.listview_item_episode);

            //ListViewItem data = listArr.get(position);


            //imageView.setImageResource();
            Picasso.with(context).load(imgUrl).into(imageView);
            tv_title.setText(titleArr.get(position));
            //tv_episode.setText(data.getEpisode());

        }

        return convertView;
    }

    // Filter Class
    public void filter(String charText) {
        charText = charText.toLowerCase(Locale.getDefault());
        titleArr.clear();
        if (charText.length() == 0) {
            titleArr.addAll(arraylist);
        }
        else {
            for (String wp : arraylist) {
                if (wp.toLowerCase(Locale.getDefault()).contains(charText)) {
                    titleArr.add(wp);
                }
                /*if (wp.getCountry().toLowerCase(Locale.getDefault()).contains(charText)) {
                    worldpopulationlist.add(wp);
                }*/
            }
        }
        notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        return titleArr.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }
}
