package com.innovalic.innocomics.model;

/**
 * Created by Administrator on 2016-07-04.
 */
public class ListViewItem {
    String ImgUrl;
    String title;

    public ListViewItem(String imgUrl, String title) {
        ImgUrl = imgUrl;
        this.title = title;
    }

    public String getImgUrl() {
        return ImgUrl;
    }

    public void setImgUrl(String imgUrl) {
        ImgUrl = imgUrl;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}
