package com.innovalic.innocomics.model;

/**
 * Created by Administrator on 2016-07-04.
 */
public class ComicViewListViewItem {
    String imgUrl;

    public ComicViewListViewItem(String imgUrl) {
        this.imgUrl = imgUrl;
    }

    public String getImgUrl() {
        return imgUrl;
    }

    public void setImgUrl(String imgUrl) {
        this.imgUrl = imgUrl;
    }
}
