package com.innovalic.innocomics.model;

/**
 * Created by Administrator on 2016-07-04.
 */
public class GridViewItem {
    String imgUrl;
    String title;
    String listUrl;

    public GridViewItem(String imgUrl, String title, String listUrl) {
        this.imgUrl = imgUrl;
        this.title = title;
        this.listUrl = listUrl;
    }

    public String getListUrl() {
        return listUrl;
    }

    public void setListUrl(String listUrl) {
        this.listUrl = listUrl;
    }

    public String getImgUrl() {
        return imgUrl;
    }

    public void setImgUrl(String imgUrl) {
        this.imgUrl = imgUrl;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}
