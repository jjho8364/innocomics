package com.innovalic.innocomics.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.innovalic.innocomics.R;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

/**
 * Created by Administrator on 2016-07-04.
 */
public class ComicViewListViewAdapter extends BaseAdapter {
    Context context;
    private LayoutInflater inflater;
    private ArrayList<String> imgUrl;

    public ComicViewListViewAdapter(Context context, LayoutInflater inflater, ArrayList<String> imgUrl) {
        this.context = context;
        this.inflater = inflater;
        this.imgUrl = imgUrl;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        convertView = inflater.inflate(R.layout.comicviewlistitem, null);
        if(convertView != null){

            ImageView imageView = (ImageView)convertView.findViewById(R.id.comicview_imageview);
            //imageView.setImageResource();
            Picasso.with(context).load(imgUrl.get(position)).into(imageView);
            //tv_title.setText(titleArr.get(position));
            //tv_episode.setText(data.getEpisode());

        }

        return convertView;
    }

    @Override
    public int getCount() {
        return imgUrl.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

}
